﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CameraController : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetAxis("Mouse ScrollWheel") > 0)
        {
            if (transform.position.y > 2)
            {
                Vector3 newPosition = transform.position;
                newPosition.y -= 0.5f;
                GetComponent<Transform>().position = newPosition;
            }
        }

        if (Input.GetAxis("Mouse ScrollWheel") < 0)
        {
            Vector3 newPosition = transform.position;
            newPosition.y += 0.5f;
            GetComponent<Transform>().position = newPosition;
        }

        if (Input.GetKey(KeyCode.A))
        {
            Vector3 newPosition = transform.position;
            newPosition.x -= 0.5f;
            GetComponent<Transform>().position = newPosition;
        }

        if (Input.GetKey(KeyCode.D))
        {
            Vector3 newPosition = transform.position;
            newPosition.x += 0.5f;
            GetComponent<Transform>().position = newPosition;
        }

        if (Input.GetKey(KeyCode.W))
        {
            Vector3 newPosition = transform.position;
            newPosition.z += 0.5f;
            GetComponent<Transform>().position = newPosition;
        }

        if (Input.GetKey(KeyCode.S))
        {
            Vector3 newPosition = transform.position;
            newPosition.z -= 0.5f;
            GetComponent<Transform>().position = newPosition;
        }

    }

	public void ExitScene ()
	{
		/// string previousScene = SceneManager.GetActiveScene().name;
		SceneManager.LoadScene("RegionMap");
	}
}
