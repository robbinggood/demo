﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private float destX, destZ;
    public Rigidbody rb;

    // Use this for initialization
    void Start()
    {
        destX = -1;
        destZ = -1;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        if (destX > 0)
        {
            Vector3 currPosition = transform.position;
            if (Mathf.Abs(destX - currPosition.x) + Mathf.Abs(destZ - currPosition.z) < 1) // almost there
            {
                rb.velocity = Vector3.zero;
                currPosition.x = destX;
                currPosition.z = destZ;
                transform.position = currPosition;
                destX = -1;
                destZ = -1;
            }
            else
            {
                if (rb.velocity.sqrMagnitude < 0.5)
                {
                    currPosition.x -= destX;
                    currPosition.y = 0;
                    currPosition.z -= destZ;
                    rb.velocity = -2 * currPosition.normalized;
                }
            }
        }
    }

    public bool GoTo(float newX, float newZ)
    {
        if (destX < 0)
        {
            destX = newX;
            destZ = newZ;
            return true;
        }
        else
            return false;
    }
}
