﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionController : MonoBehaviour
{
    public PlayerController player;
    public GameObject selection;
    Vector3 oldHit;

    // Use this for initialization
    void Start ()
    {
        oldHit = Input.mousePosition;
    }
	
	// Update is called once per frame
	void Update ()
    {
        HighLightHover();

        if (Input.GetMouseButtonDown(0))
            MovePlayer();
    }

    private void HighLightHover()
    {
        if (Camera.main)
        {
            RaycastHit groundHit;
            bool didHit = Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out groundHit, maxDistance: 100, layerMask: LayerMask.GetMask("GroundHitLayer"));

            if (didHit)
            {
                oldHit = groundHit.point;

                Vector3 higlightedCenter;
                higlightedCenter.x = Mathf.Round(oldHit.x);
                higlightedCenter.y = 1;
                higlightedCenter.z = Mathf.Round(oldHit.z);
                selection.transform.position = higlightedCenter;
            }
        }
    }

    private void MovePlayer()
    {
        player.GoTo(oldHit.x, oldHit.z);
    }
}
